job "try-lxc" {
    datacenters = ["dc1"]
    type        = "batch"

    group "try-lxc" {
        count = 1

        task "example" {
            ## https://www.nomadproject.io/docs/drivers/external/lxc.html
            driver = "lxc"
            config {
                log_level = "info"
                verbosity = "verbose"
                template = "/usr/share/lxc/templates/lxc-debian"

                 ## ExtraArgs are passed through template_args:
#                template_args = [
#                ]

                volumes = [
                    # Use absolute paths to mount arbitrary paths on the host
                    #"/path/on/host:path/in/container",

                    # Use relative paths to rebind paths already in the allocation dir
                    #"relative/to/task:also/in/container"
                ]

            }
            resources {
                cpu    = 500
                memory = 256
            }
        }
    }
}
